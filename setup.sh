#!/bin/bash

echo "Configuring git..."
git config --global user.name "Gunnar K. Halvorsen"
git config --global user.email "gunnar@ghalv.no"

echo "Generating GPG key..."
echo "Please follow the prompts to generate your GPG key."
gpg --full-generate-key
read -p "Press [Enter] once you have completed the GPG key generation process."

echo "Changing .gnupg permissions..."
sudo chmod 700 .gnupg/

echo "Initializing pass..."
pass init gunnar@ghalv.no

echo "Generating SSH key..."
echo "Please follow the prompts to generate your SSH key."
ssh-keygen
read -p "Press [Enter] once you have completed the SSH key generation process."

echo "pam-gnupg"
sudo echo -e "auth     optional  pam_gnupg.so store-only\nsession  optional  pam_gnupg.so" >> /etc/pam.d/system-local-login

echo "Kjør og lagre output"
gpg -K --with-keygrip | grep -oP 'Keygrip = \K.*' | tail -n 1 > ~/.pam-gnupg

echo "Enabling cronie"
sudo ln -s /etc/runit/sv/socklog /run/runit/service
sudo sv start cronie

echo "Symlink pacman hook"
sudo ln -s $HOME/dox/pacman/statusbar.hook /usr/share/libalpm/hooks/statusbar.hook

echo "Symlinks to from .local/bin to /usr/bin"
sudo rm /usr/bin/spotify /usr/bin/passmenu
sudo ln -s $HOME/.local/bin/spotscript /usr/bin/spotify
sudo ln -s $HOME/.local/bin/passmenu /usr/bin/passmenu

echo "Cronjobber"
sudo ln -s /etc/runit/sv/cronie /run/runit/service/cronie
sv start cronie

echo "Add email"
pass init gunnar@ghalv.no
mw -a gunnar@ghalv.no
mw -t 3

echo "add PIM"
mkdir git && git clone https://gitlab.com/ghalv/PIM $HOME/git/PIM

--------

cron:
0 */1 * * * /home/gunnar/.local/bin/cron/newsup

sudo cron:
0 */8 * * * /home/gunnar/.local/bin/cron/checkup
