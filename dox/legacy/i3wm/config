# CONFIG FOR ARCH
# -- GUNNAR 18 --

for_window [class="^.*"] border pixel 0
font pango:noto sans 12
set $mod Mod1
set $term --no-startup-id urxvt

# Autostart
exec_always --no-startup-id "feh --bg-fill ~/Pictures/Wallpapers/wp3.jpeg"
exec_always --no-startup-id "setxkbmap no -option terminate:ctrl_alt_bksp -option kpdl:dot -option caps:escape"
exec_always --no-startup-id "numlockx on"
#exec_always --no-startup-id "xinput --disable 'SynPS/2 Synaptics TouchPad'"
exec_always --no-startup-id "nm-applet"
exec_always --no-startup-id "compton --xrender-sync-fence"
exec_always --no-startup-id "dropbox start"
exec_always --no-startup-id "unclutter -idle 1"
exec	--no-startup-id "Desktop-Bridge --cli"
exec	--no-startup-id "redshift"

# Keybindings
bindsym Ctrl+$mod+l exec i3lock -c 000000
bindsym Ctrl+$mod+x exec --no-startup-id ~/.scripts/prompt "Shutdown computer?" "poweroff"
bindsym Ctrl+$mod+s exec i3lock -c 000000 && systemctl suspend
bindsym $mod+F1 exec --no-startup-id groff -kejpt -mom ~/git/dotfiles/readme.mom -Tpdf | zathura -
bindsym $mod+F2 exec --no-startup-id zathura ~/git/Bryggeboken/main.pdf
bindsym $mod+a [instance="math"] scratchpad show; [instance="math"] move position center
bindsym $mod+b exec $term -e lynx http://www.gutenberg.org/browse/scores/top
bindsym $mod+c exec $term -e calcurse #signal-desktop
bindsym $mod+e exec $term -e neomutt
bindsym $mod+f exec $term -e ranger
bindsym $mod+g exec firefox
bindsym $mod+i exec $term -e mosh ba7.ux.uis.no tmux attach-session
bindsym $mod+m exec rstudio-bin
bindsym $mod+n exec $term -e newsboat -r
bindsym $mod+p exec ~/.scripts/passmenu --type
bindsym $mod+r exec rstudio #rtv -s Linux #r -q
bindsym $mod+s exec $term -e htop
#bindsym $mod+t exec $term -e tty-clock
bindsym $mod+u exec $term -e vim ~/git/dotfiles/i3wm/config +1
bindsym $mod+w exec $term -hold -e curl wttr.in/Ullandhaug?M
bindsym $mod+x exec qutebrowser #firefox
bindsym $mod+z exec $term -e sc-im
bindsym Print exec "scrot -e 'mv $f ~/Pictures/Screenshots/'"

# Arithmetic dropdown
for_window [instance="math"] floating enable
for_window [instance="math"] resize set 800 400
for_window [instance="math"] move scratchpad
#for_window [instance="math"] border pixel 5
exec --no-startup-id $term -name math -fn "xft:IBM 3270:pixelsize=24" -e ~/.scripts/dropdowncalc

# Audio
bindsym Ctrl+Down 		exec --no-startup-id amixer -q -D default sset Master 5%- # Decrease volume
bindsym Ctrl+Up   		exec --no-startup-id amixer -q -D default sset Master 5%+ # Increase volume
bindsym XF86AudioLowerVolume 	exec --no-startup-id amixer -q -D default sset Master 5%- # Decrease volume
bindsym XF86AudioRaiseVolume 	exec --no-startup-id amixer -q -D default sset Master 5%+ # Increase volume
bindsym XF86AudioMute 		exec --no-startup-id amixer -D default set Master 1+ toggle # Mute volume

# ------DEFAULT-------
# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
# font pango:DejaVu Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn't scale on retina/hidpi displays.

# use these keys for focus, movement, and resize directions when reaching for
# the arrows is not convenient
set $up l
set $down k
set $left j
set $right semicolon

# use Mouse+Mod1 to drag floating windows to their wanted position
floating_modifier Mod1

# start a terminal
bindsym $mod+Return exec $term

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec dmenu_run
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym Mod1+d exec --no-startup-id i3-dmenu-desktop

# change focus
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# alternatively, you can use the cursor keys:
bindsym Mod1+Left focus left
bindsym Mod1+Down focus down
bindsym Mod1+Up focus up
bindsym Mod1+Right focus right

# move focused window
bindsym Mod1+Shift+$left move left
bindsym Mod1+Shift+$down move down
bindsym Mod1+Shift+$up move up
bindsym Mod1+Shift+$right move right

# alternatively, you can use the cursor keys:
bindsym Mod1+Shift+Left move left
bindsym Mod1+Shift+Down move down
bindsym Mod1+Shift+Up move up
bindsym Mod1+Shift+Right move right

# split in horizontal orientation
bindsym Mod1+h split h

# split in vertical orientation
bindsym Mod1+v split v

# enter fullscreen mode for the focused container
bindsym Mod1+Shift+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym Mod1+Shift+s layout stacking
bindsym Mod1+Shift+w layout tabbed
bindsym Ctrl+Shift+e layout toggle split

# toggle tiling / floating
bindsym Mod1+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym Mod1+space focus mode_toggle

# focus the parent container
bindsym Mod1+Shift+a focus parent

# focus the child container
#bindsym Mod1+d focus child

# move the currently focused window to the scratchpad
bindsym Mod1+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym Mod1+minus scratchpad show

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"


# switch to workspace
bindsym Mod1+1 workspace $ws1
bindsym Mod1+2 workspace $ws2
bindsym Mod1+3 workspace $ws3
bindsym Mod1+4 workspace $ws4
bindsym Mod1+5 workspace $ws5
bindsym Mod1+6 workspace $ws6
bindsym Mod1+7 workspace $ws7
bindsym Mod1+8 workspace $ws8
bindsym Mod1+9 workspace $ws9
bindsym Mod1+0 workspace $ws10

# move focused container to workspace
bindsym Mod1+Shift+1 move container to workspace $ws1
bindsym Mod1+Shift+2 move container to workspace $ws2
bindsym Mod1+Shift+3 move container to workspace $ws3
bindsym Mod1+Shift+4 move container to workspace $ws4
bindsym Mod1+Shift+5 move container to workspace $ws5
bindsym Mod1+Shift+6 move container to workspace $ws6
bindsym Mod1+Shift+7 move container to workspace $ws7
bindsym Mod1+Shift+8 move container to workspace $ws8
bindsym Mod1+Shift+9 move container to workspace $ws9
bindsym Mod1+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym Mod1+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym Mod1+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym Ctrl+Mod1+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym $left       resize shrink width 10 px or 10 ppt
        bindsym $down       resize grow height 10 px or 10 ppt
        bindsym $up         resize shrink height 10 px or 10 ppt
        bindsym $right      resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left        resize shrink width 10 px or 10 ppt
        bindsym Down        resize grow height 10 px or 10 ppt
        bindsym Up          resize shrink height 10 px or 10 ppt
        bindsym Right       resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or Mod1+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        # bindsym Mod1+r mode "default"
}

#bindsym Mod1+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        status_command i3status -c ~/.config/i3/i3status.conf
}

#######################################################################
# automatically start i3-config-wizard to offer the user to create a
# keysym-based config which used their favorite modifier (alt or windows)
#
# i3-config-wizard will not launch if there already is a config file
# in ~/.i3/config.
#
# Please remove the following exec line:
#######################################################################
exec i3-config-wizard
# vim:filetype=i3
