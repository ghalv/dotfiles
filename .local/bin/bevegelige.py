#!/usr/bin/env python3
import argparse
import datetime

def calculate_easter(year):
    a = year % 19
    b = year // 100
    c = year % 100
    d = (19 * a + b - b // 4 - ((b - (b + 8) // 25 + 1) // 3) + 15) % 30
    e = (32 + 2 * (b % 4) + 2 * (c // 4) - d - (c % 4)) % 7
    f = d + e - 7 * ((a + 11 * d + 22 * e) // 451) + 114
    month = f // 31
    day = f % 31 + 1
    return datetime.date(year, month, day)

parser = argparse.ArgumentParser()
parser.add_argument("year", nargs='?', type=int, default=datetime.datetime.now().year, help="Year for which Easter's date must be calculated")
args = parser.parse_args()

year = args.year
easter = calculate_easter(year)
print(f"Easter is on {easter}")
